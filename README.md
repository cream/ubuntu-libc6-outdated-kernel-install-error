# Linux Kernel 6.5 or later fails to install because unmet dependencies libc6 > 2.38 or 2.xx

>Issue Thread(s):
    1. https://ubuntuforums.org/showthread.php?t=2490234&p=14155919
    2. https://bugs.launchpad.net/ubuntu/+source/linux/+bug/1926938

## Example Error:
``` 
linux-headers-6.4.13-060413-generic depends on libc6 (>= 2.38); however:
Version of libc6:amd64 on system is 2.35-0ubuntu3.1.
``` 

# Solution : using the Linux Zabbly APT repository

Original source : https://www.linuxcapable.com/how-to-install-latest-linux-kernel-on-debian-linux/

``` 
sudo apt update && sudo apt upgrade
sudo apt install lsb-release software-properties-common apt-transport-https ca-certificates curl
curl -fSsL https://pkgs.zabbly.com/key.asc | gpg --dearmor | sudo tee /usr/share/keyrings/linux-zabbly.gpg > /dev/null
``` 

Now we need to import the Linux Zabbly Kernel APT repository, it was meant for Debian 11 (Bullseye) or Debian 12 (Bookworm) but both just works on Ubuntu too, choose any of the two :

``` 
codename=$(lsb_release -sc) && echo deb [arch=amd64,arm64 signed-by=/usr/share/keyrings/linux-zabbly.gpg] https://pkgs.zabbly.com/kernel/stable bullseye main | sudo tee /etc/apt/sources.list.d/linux-zabbly.list
``` 
``` 
codename=$(lsb_release -sc) && echo deb [arch=amd64,arm64 signed-by=/usr/share/keyrings/linux-zabbly.gpg] https://pkgs.zabbly.com/kernel/stable bookworm main | sudo tee /etc/apt/sources.list.d/linux-zabbly.list
``` 
``` 
sudo apt update
sudo apt install linux-zabbly linux-libc-dev
sudo reboot
``` 

* Note : You need to disable secure boot in bios/uefi because obviously you are using a third party source as THE KERNEL
* You have installed the latest kernel! :)